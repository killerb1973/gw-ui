import { registerLocaleData } from '@angular/common'
import { HttpClientModule } from '@angular/common/http'
import localeFr from '@angular/common/locales/hu'
import { LOCALE_ID, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { AdvertListComponent } from './components/advert-list/advert-list.component'
import { AdvertComponent } from './components/advert/advert.component'
import { ProductSearchComponent } from './components/product-search/product-search.component'
import { ProductStatComponent } from './components/product-stat/product-stat.component';
import { ProductAdvertComponent } from './components/product-advert/product-advert.component';
import { AdvertFilterComponent } from './components/advert-filter/advert-filter.component'

registerLocaleData(localeFr, 'hu')

@NgModule({
    declarations: [
        AppComponent,
        AdvertListComponent,
        ProductStatComponent,
        ProductSearchComponent,
        AdvertComponent,
        ProductAdvertComponent,
        AdvertFilterComponent
    ],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
    providers: [{ provide: LOCALE_ID, useValue: 'hu' }],
    bootstrap: [AppComponent]
})
export class AppModule {}
