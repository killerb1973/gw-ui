import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AdvertListComponent } from './components/advert-list/advert-list.component'
import { ProductStatComponent } from './components/product-stat/product-stat.component'

const routes: Routes = [
    { path: '', redirectTo: '/tophits', pathMatch: 'full' },
    { path: 'tophits', component: AdvertListComponent },
    { path: 'product', redirectTo: '/tophits', pathMatch: 'full' },
    { path: 'product/:id', component: ProductStatComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
