declare module '@app/domain' {
    interface Advert {
        img: string
        link: string
        price: number
        avrUsedPrice: number
        bestNewPrice: number
        productBrand: string
        productCode: string
        productName: string
        productId: string
        reason: string
    }

    interface ProductAdvert {
        img: string
        link: string
        price: number
        source: string
    }

    interface PriceStat {
        min: number
        max: number
        avg: number
        count: number
    }

    interface ProductStatistic {
        id: string
        product: Product
        newPrice: PriceStat
        usedPrice: PriceStat
        newAdverts: ProductAdvert[]
        usedAdverts: ProductAdvert[]
    }

    interface Product {
        id: string
        brand: string
        code: string
        name: string
        link: string
        img: string
    }

    interface ShippingFrom {
        id: string
        name: string
    }
}
