import {HttpClient, HttpHeaders} from '@angular/common/http'
import {EventEmitter, Injectable, Output} from '@angular/core'
import {Advert} from '@app/domain'
import {Observable, of} from 'rxjs'
import {catchError, tap} from 'rxjs/operators'

import {environment} from '../../environments/environment'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class AdvertService {
  private advertsUrl = environment.baseUrl + '/api/tophits?'
  shippingLocation = 'ALL';
  currency = 'HUF';
  margin = 90;

  @Output() reloadAdverts: EventEmitter<null> = new EventEmitter<null>();

  getAdverts(): Observable<Advert[]> {
    return this.http.get<Advert[]>(this.advertsUrl + 'margin=' + this.margin + '&shippingFrom=' + this.shippingLocation + '&currency=' + this.currency)
      .pipe(tap(_ => console.log('fetched adverts')),
      catchError(this.handleError('getHeroes', []))
    )
  }

  filterChanged(shippingLocation: string) {
    this.shippingLocation = shippingLocation;
    this.reloadAdverts.emit();
  }

  constructor(private http: HttpClient) {
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
