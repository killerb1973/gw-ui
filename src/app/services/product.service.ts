import { HttpClient } from '@angular/common/http'
import {EventEmitter, Injectable, Output} from '@angular/core'
import { Observable, of } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'

import { environment } from '../../environments/environment'
import { Product, ProductStatistic } from '@app/domain'

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private productUrl = environment.baseUrl + '/api/product'
    shippingFrom = 'ALL'

  @Output() reloadProduct: EventEmitter<null> = new EventEmitter<null>();

    constructor(private http: HttpClient) {}

    searchProducts(term: string): Observable<Product[]> {
        if (!term.trim()) {
            // if not search term, return empty hero array.
            return of([])
        }
        return this.http
            .get<Product[]>(`${this.productUrl}?query=${term}`)
            .pipe(
                tap(_ => console.log(`found products matching "${term}"`)),
                catchError(this.handleError('searchHeroes', []))
            )
    }

    setShippingFrom(id: string) {
      this.shippingFrom = id;
      this.reloadProduct.emit();
    }

    getProduct(id: string): Observable<ProductStatistic> {
        const url = `${this.productUrl}/stat/${id}?currency=HUF&shippingFrom=` + this.shippingFrom
        return this.http.get<ProductStatistic>(url).pipe(
            tap(_ => console.log(`fetched product id=${id}`)),
            catchError(this.handleError<ProductStatistic>(`getHero id=${id}`))
        )
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error) // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T)
        }
    }
}
