import { Component, Input } from '@angular/core'
import {Advert} from '@app/domain'
import {ProductService} from '../../services/product.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-advert',
    templateUrl: './advert.component.html',
    styleUrls: ['./advert.component.scss']
})
export class AdvertComponent {
    @Input()
    advert: Advert

    @Input()
    embedded: boolean

  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

    get photoUrl(): string {
        return this.advert.img &&
            this.advert.img !== 'https://ir.ebaystatic.com/cr/v/c1/s_1x2.gif'
            ? this.advert.img
            : 'assets/img/fallback-image.jpg'
    }

    openAdvert = () => {
        window.open(this.advert.link, '_blank')
    }

    openProduct = () => {
        this.router.navigateByUrl(`/product/${this.advert.productId}`)
    }
}
