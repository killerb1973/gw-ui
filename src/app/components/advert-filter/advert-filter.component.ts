import {Component, OnInit} from '@angular/core';
import {ShippingFrom} from '@app/domain';
import {AdvertService} from '../../services/advert.service';
import {ProductService} from '../../services/product.service';

@Component({
  selector: 'app-advert-filter',
  templateUrl: './advert-filter.component.html',
  styleUrls: ['./advert-filter.component.scss']
})
export class AdvertFilterComponent implements OnInit {
  shippingLocations: ShippingFrom[]
  searchFocused: boolean

  clickIn = false;

  clickInside() {
    this.clickIn = true;
    setTimeout(() => this.clickIn = false, 190);
  }

  constructor(private advertService: AdvertService,
              private productService: ProductService) {
  }

  ngOnInit() {
    this.shippingLocations = this.getShippingFromLocation()
  }

  refreshAdverts(shippingLocation) {
    this.clickInside()
    console.log(shippingLocation)
    this.advertService.filterChanged(shippingLocation);
    this.productService.setShippingFrom(shippingLocation);
    this.searchFocused = false;
  }

  onFocusChange = (focused: boolean) => {
    if (!focused) {
      setTimeout(() => {
        if (!this.clickIn) {
          console.log(focused)
          this.searchFocused = false
        }
      }, 150);
    } else {
      this.searchFocused = true
    }
  }

  getShippingFromLocation() {
    return [
      {id: 'ALL', name: 'World wide'},
      {id: 'EUROPE', name: 'Europe only'},
      {id: 'HUN', name: 'Local only'}
    ];
  }

}
