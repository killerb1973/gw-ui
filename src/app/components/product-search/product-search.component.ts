import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Product } from '@app/domain'
import { Subject } from 'rxjs'
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators'

import { ProductService } from '../../services/product.service'

@Component({
    selector: 'app-product-search',
    templateUrl: './product-search.component.html',
    styleUrls: ['./product-search.component.scss']
})
export class ProductSearchComponent implements OnInit {
    products: Product[]
    searchFocused: boolean

    private searchTermChange = new Subject<string>()

    constructor(
        private productService: ProductService,
        private router: Router
    ) {}

    ngOnInit() {
        this.searchTermChange
            .pipe(
                // wait 300ms after each keystroke before considering the term
                debounceTime(300),

                // ignore new term if same as previous term
                distinctUntilChanged(),

                // switch to new search observable each time the term changes
                switchMap((term: string) =>
                    this.productService.searchProducts(term)
                )
            )
            .subscribe(products => (this.products = products))
    }

    onSearch = (term: string) => {
        this.searchTermChange.next(term)
    }

    onFocusChange = (focused: boolean) => {
        if (!focused) {
            setTimeout(() => (this.searchFocused = focused), 150)
        } else {
            this.searchFocused = focused
        }
    }

    openProduct = (product: Product) => {
        this.router.navigateByUrl(`/product/${product.id}`)
    }
}
