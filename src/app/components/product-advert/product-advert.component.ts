import {Component, Input} from '@angular/core';
import {ProductAdvert} from '@app/domain';

@Component({
  selector: 'app-product-advert',
  templateUrl: './product-advert.component.html',
  styleUrls: ['./product-advert.component.scss']
})
export class ProductAdvertComponent {
  @Input()
  advert: ProductAdvert

  @Input()
  embedded: boolean

  get photoUrl(): string {
    return this.advert.img &&
    this.advert.img !== 'https://ir.ebaystatic.com/cr/v/c1/s_1x2.gif'
      ? this.advert.img
      : 'assets/img/fallback-image.jpg'
  }

  openAdvert = () => {
    window.open(this.advert.link, '_blank')
  }

}
