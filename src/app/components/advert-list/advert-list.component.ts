import { Component, OnInit } from '@angular/core'
import { Advert } from '@app/domain'

import { AdvertService } from '../../services/advert.service'

@Component({
    selector: 'app-advert-list',
    templateUrl: './advert-list.component.html',
    styleUrls: ['./advert-list.component.scss']
})
export class AdvertListComponent implements OnInit {
    adverts: Advert[]

    constructor(private advertService: AdvertService) {}

    ngOnInit() {
        this.getAdverts();
        this.advertService.reloadAdverts.subscribe( () => (this.getAdverts()));
    }

    getAdverts(): void {
        this.advertService
            .getAdverts()
            .subscribe(adverts => (this.adverts = adverts))
    }
}
