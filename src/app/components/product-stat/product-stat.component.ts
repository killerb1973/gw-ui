import 'zingchart'

import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { ProductStatistic } from '@app/domain'

import { ProductService } from '../../services/product.service'

declare var zingchart: any

@Component({
    selector: 'app-product-stat',
    templateUrl: './product-stat.component.html',
    styleUrls: ['./product-stat.component.scss']
})
export class ProductStatComponent implements OnInit {
    productStat: ProductStatistic
    loading: boolean

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private productService: ProductService
    ) {
        this.loading = false
    }

    ngOnInit() {
        this.getProductStat()

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.getProductStat()
            }
        })

      this.productService.reloadProduct.subscribe( () => (this.getProductStat()));
    }

    get photoUrl(): string {
        return this.productStat.product &&
            this.productStat.product.img !==
                'https://ir.ebaystatic.com/cr/v/c1/s_1x2.gif'
            ? this.productStat.product.img
            : 'assets/img/fallback-image.jpg'
    }

    private getProductStat = () => {
        const id = this.route.snapshot.paramMap.get('id')

        this.loading = true

        this.productService.getProduct(id).subscribe(product => {
            this.productStat = product
            this.loading = false

            setTimeout(() => this.drawChart(product))
        })
    }

    private mapProductDataToChartData: (
        productStat: ProductStatistic
    ) => any = productStat => {
        if (productStat.newPrice && productStat.usedPrice) {
            const {
                min: newMin,
                max: newMax,
                avg: newAvg
            } = productStat.newPrice
            const {
                min: usedMin,
                max: usedMax,
                avg: usedAvg
            } = productStat.usedPrice

            return [
                {
                    values: [
                        Math.max(newMax - newMin, 400),
                        Math.max(usedMax - usedMin, 400)
                    ],
                    offsetValues: [newMin, usedMin],
                    'background-color': '#66a6ff',
                    'data-price-min': [newMin, usedMin],
                    'data-price-max': [newMax, usedMax],
                    'data-price-avg': [newAvg.toFixed(0), usedAvg.toFixed(0)],
                    'data-type': [
                        'New product price interval',
                        'Used product price interval'
                    ]
                }
            ]
        }

        return []
    }

    private drawChart = (productStat: ProductStatistic) => {
        const chartSeries = this.mapProductDataToChartData(productStat)

        const zingchartConfig = {
            id: 'chart-container',
            data: {
                type: 'hbar',
                'background-color': 'transparent',
                legend: {
                    visible: false
                },
                tooltip: {
                    text:
                        '<span style="font-size: 14px; font-weight: bold;">%data-type</span><br />' +
                        'Min. price: %data-price-min HUF<br />Max. price: %data-price-max HUF<br />' +
                        'Avg. price: %data-price-avg HUF',
                    'background-color': '#231f20',
                    'border-width': 0,
                    'border-radius': 5,
                    'font-size': 13,
                    'line-height': 16,
                    'padding-bottom': 15
                },
                plotarea: {
                    margin: '30 40 60'
                },
                plot: {
                    'border-radius': 8
                },
                scaleX: {
                    labels: ['New', 'Used']
                },
                scaleY: {
                    label: {
                        text: 'Price comparison (HUF)',
                        fontSize: 14
                    }
                },
                series: chartSeries
            },
            width: '100%',
            height: 300
        }

        zingchart.TOUCHZOOM = 'pinch'

        zingchart.render(zingchartConfig)
    }
}
